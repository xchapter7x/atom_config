# PivotalCF:
--
--
--
--
### Orgs, Spaces, and Roles
==========================================

---

Agenda
------

- Orgs, spaces, roles
- Account quotas
- Administering Users

![right 100%](./assets/pcf-orgs-spaces.png)

---

![right 70%](./assets/universal_imports_logo.png)

Dear James Blonde,

On behalf of Universal Imports I would like to welcome you to the new agent training program.

Information regarding who we are and our structure will be provided on a need to know basis.

Enclosed you will find a encoded message containing information vital to your mission. Decode the message to reveal the key. Good luck.

Regards,
The Administrator

---

Universal Imports
---------------------
**Things to remember:**

- Here at Universal Imports we are all one big happy **Org**.

- For your own safety, or in case you are a double agent, we will keep you in the trainee **Space**, for now.

- You're not a meter-maid, but you will have a **quota**.

- As you grow into a star 00-importer we will give you clearance to access more top secret **Spaces** and expand your **Role** in the **Org**anization.

![fit](./assets/universal_imports_logo.png)

---

Orgs:
-----
--
--
#### "Here at Universal Imports we are all one big happy **Org**"
--
--
## What's an Org?
===================

---

Orgs:
-----
The top tier in the organizational hierarchy. Users at this level are grouped by:

- Quotas
- Custom domains

![right 75%](./assets/org_is_universal.png)

---

Orgs:
-----

CLI
------------

View org details

```bash
$ cf orgs
$ cf org <name>
  domains: mycompany.com
  quota:   runaway (153600M memory limit, 1000 routes, -1 services, paid services allowed)
  spaces:  development, training
```

---

Orgs:
-----

CLI
------------

### Create/rename/delete orgs:

```
$ cf create-org <name>
$ cf rename-org <name> <new name>
$ cf delete-org <name>
```

---

![autoplay loop fit](./assets/create-org-cli.mov)

---

Orgs:
-----
GUI
---

#### Command line a little too retro?

Work with orgs via the GUI:
---------------------------

---

![autoplay loop fit](./assets/create-org-gui.mov)

---

Spaces:
-----
--
--
#### "For your own safety, or in case you are a double agent, we will keep you in the trainee **Space**, for now."
--
--
## What's a Space?
===================

---

![fit](./assets/org_spaces_is_universal.png)

---

Spaces:
-----
The tier immediately beneath "org". Users at this level have:

- Custom routes
- Scoped resources (to an application, team, and/or environment)
- Shared space for application development, deployment, and maintenance

![right 75%](./assets/org_spaces_is_universal.png)

---

Spaces:
-----

CLI
--------------
### Create/rename/delete spaces:

```
$ cf spaces

$ cf space <space name>

$ cf create-space <new space name>

$ cf rename-space <current name for space> <new name for space>

$ cf delete-space <space name>
```
---

Spaces:
----------

GUI
------

Work with spaces via the GUI:
-----------------------------

![right 75%](./assets/got_mouse.png)

---

![autoplay loop fit](./assets/spaces-gui.mov)

---

Orgs:
-----
--
--

#### "You're not a meter-maid, but you will have a **quota**."

--
--
## What's a Quota?
===================

---

Org Quotas:
------
--
* Constrain an organization

* Protect against runaway expenses

---

Org Quotas:
----------
CLI
-----------------------

### Quotas can be viewed, created/deleted, and set/updated:

```
$ cf quota <quota name>

$ cf create-quota <quota name> -m <memory> \
-r <number of routes> \
-s <number of service instances>

$ cf delete-quota <quota name>

$ cf set-quota <org name> <quota name>

$ cf update-quota <quota name> -r <number of routes>
```

---

![fit autoplay loop](./assets/create-quotas-cli.mov)

---

Orgs / Spaces:
-----
--
--

![70%](./assets/org_spaces_is_universal.png)

--
--
--
## Who uses these orgs & spaces?
======================

---

# Users:

## Like James Blonde

![right 70%](./assets/users.png)

---

![left 70%](./assets/users.png) ![right 70%](./assets/org_spaces_is_universal.png)

---

Users:
------
CLI
---

How To Create a User
--------------------

Log in using admin credentials:

```bash
$ cf login
```

Create the new user's account:

```bash
$ cf create-user <email@domain.com> <password>
```

---

Users:
------
CLI
---

How To Change a Password
------------------------

```bash
$ cf passwd
```

How to change your own password from a temporary password:

```bash
$ cf target <api domain>
$ cf login --username <email@domain.com> --password <temp password>
$ cf passwd --password <new password>
```

---

Users:
-----
GUI
---

#### All this keyboard action giving you Carpal Tunnel?

Work with users via the GUI:
----------------------------

---

![autoplay loop fit](./assets/users-roles-gui.mov)

---

Roles:
-----
--
--
#### "As you grow into a star 00-importer we will give you clearance to access more top secret **Spaces** and expand your **Role** in the **Org**anization"
--
--
## What's a Role?
======================

---

Roles:
------

Roles define activities a user can perform and their scope. An individual User can have multiple roles for the various Orgs and Spaces that exist in your PaaS.

---

Roles:
------
--
--
"Regards, The Administrator"
--
--
# Who is the administrator?

---

Roles:
------

Cloud Foundry does not have a dedicated admin role. Instead, users are assigned one or more org/space roles that control what actions the user can take.

---

Org Roles:
-----
--
--
--
--
## What role types are there in an Org?
======================

---

Org Roles:
------------

**OrgManager**: Invite and manage users, select and change plans, and set spending limits

**BillingManager**: Create and manage the billing account and payment info

**OrgAuditor**: Read-only access to org info and reports

---

![fit](./assets/roles.png)

---

Org Roles:
----------

CLI
---

#### Org roles can be set, unset, or viewed in the CLI:

```
$ cf org-users <name> [-a]
$ cf set-org-role <user name> <org name> <role>
$ cf unset-org-role <user name> <org name> <role>
```

---

![fit autoplay loop](./assets/org-roles.mov)

---

Space Roles:
-----
--
--
--
--
## Yup, Spaces have roles too!
======================

---

Space Roles:
-------------

**SpaceManager**: Add/manage users, enable/disable features Space Auditor capabilities

**SpaceDeveloper**: manage applications and services

**SpaceAuditor**: view but not edit the space

---

![fit](./assets/roles.png)

---

Space Roles:
-------------
CLI
---

### Space roles can be set, unset, or viewed in the CLI:

```
$ cf space-users <org name> <space name>

$ cf set-space-role <userEmail@domain.com> <org name> <space name> <role>

$ cf unset-space-role <userEmail@domain.com> <org name> <space name> <role>
```

---

Org / Space Roles:
-----
GUI
---

### Have we mentioned there is a GUI interface?

Work with roles via the GUI:

---

![autoplay loop fit](./assets/users-roles-gui.mov)
