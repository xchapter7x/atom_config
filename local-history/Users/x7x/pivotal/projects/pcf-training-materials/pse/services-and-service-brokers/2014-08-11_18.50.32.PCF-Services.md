# PivotalCF:
--
--
--
--
### Services
==========================================

---

Agenda
------

- Services
- Service Brokers

---

Services:
-----
--
--
--
--
## What is a Service?
===================

---

Services:
---------
#### They allow the flow of information in your applications
- Databases
- Message Queues
- Monitoring / Logging
- Hadoop instance
- Web Services / REST / SOAP
- External Resources

![right](./assets/Aqueduct_of_Segovia_08.jpg)

---

Services:
---------

#### Information in the form of a service is now a measurable and billable resource in the PaaS:

**Service types**
- Out-of-box Services
- Custom built services
- User-provided

---

Services:
-----
--
--
--
--
## So, how do I use Services?
===================

---

##How to use a Service?
![inline](./assets/app-deploy.png)

---
##How to use a Service?
behind the scenes details
![inline 130%](./assets/dev-create-bind-service.png)

---
##How to use a Service?
![inline](./assets/vcap_services.png)

---
##Managed Services
__Managed Services__ are integrated with Cloud Foundry by implementing a documented API for which the cloud controller is the client

__Service Broker__ is a component which implements the required API.*
Service brokers advertise a catalog of service offerings and service plans to Cloud Foundry, and receive calls from the Cloud Controller for five functions: fetch catalog, create, bind, unbind, and delete.
* In CF v1, Service Brokers were called Service Gateways.

---
##Service Broker API Overview
![inline](./assets/v2-broker-api.png)

---
##Service Plan
![inline](./assets/service-buy-plan.png)

---
##Service Broker Registration
- Make the service broker known to the Cloud Controller
```
cf create service-broker <broker name> <username> <password> <broker base URI>
```
Broker should ONLY allow access to those requestors it shared its credential with (Basic Auth)
See: [http://docs.gopivotal.com/pivotalcf/services/managing-service-brokers.html#register-broker](http://docs.gopivotal.com/pivotalcf/services/managing-service-brokers.html#register-broker)

---

<continued>
- Make ‘plans’ accessible to users in a specific org/space
Somewhat cumbersome: need to “hand parse” JSON to find service plan UUID as registered with the CC
See: [http://docs.gopivotal.com/pivotalcf/services/access-control.html#make-plans-public](http://docs.gopivotal.com/pivotalcf/services/access-control.html#make-plans-public)

__Need admin creds/role in order to introduce a service broker to the Cloud Controller!__

---
##Service Brokers
![inline](./assets/brokered-service-prov-bind.png)

---

##Service Broker Implementation Best Practice
Best Practice: Each binding is represented by its own credentials =>
- T=1: create service instance
  - __Neither App-1 or App-2 has access to the service instance__
- T=2: bind App-1 to service instance
  - __Only App-1 can access the service instance__
- T=3: bind App-2 to service instance
  - __Both App-1 and App-2 have access to the service instance__
- T=4 unbind App-1 from service instance
  - __Only App-2 can access the service instance__

---
##User Provided Service Instances
Typically legacy or __existing instances of a service__ (databases, queues, mail, etc) where applications connect to the __same instance__

__Credential passing__ when you need to inject the same credential set into an application

---
##User Provided Service Instances
![inline](./assets/user-provided-service-bind.png)

---
##Overview
![inline](./assets/services-env-overview.png)

---
##Summary
Deploying service instances and brokersPivotal CF only cares about the Service Broker URL.
- Several possibilities can be used for deploying services:  BOSH, vCAC, Puppet / Chef, etc  
- BOSH provides built-in resilience, VM monitoring and HA, multi-cloud support.
- For simpler cases, your own IaaS automation tool can be used.
- For quickly integration or when a Service Broker managing the service lifecycle is not desired, just use User-Provided Services.  (analogy to App Server Data Sources config)
