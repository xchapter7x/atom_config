# PivotalCF:
--
--
--
--
### Services
==========================================

---

Agenda
------

- Services
- Service Brokers

---

Services:
-----
--
--
--
--
## What is a Service?
===================

---

Services:
---------
#### They allow the flow of information in your applications
- Databases
- Message Queues
- Monitoring / Logging
- Hadoop instance
- Web Services / REST / SOAP
- External Resources

![right](./assets/Aqueduct_of_Segovia_08.jpg)

---

Services:
---------

#### Information in the form of a service is now a measurable and billable resource in the PaaS:

**Service types**
- Out-of-box Services
- Custom built services
- User-provided

---

Services:
-----
--
--
--
--
## So, how do I use Services?
===================

---

##How to use a Service?
![inline](./assets/app-deploy.png)

---
##How to use a Service?
behind the scenes details
![inline 130%](./assets/dev-create-bind-service.png)

---
##How to use a Service?
![inline](./assets/vcap_services.png)

---
##Managed Services
__Managed Services__ are integrated with Cloud Foundry by implementing a documented API for which the cloud controller is the client

__Service Broker__ is a component which implements the required API.*
Service brokers advertise a catalog of service offerings and service plans to Cloud Foundry, and receive calls from the Cloud Controller for five functions: fetch catalog, create, bind, unbind, and delete.
* In CF v1, Service Brokers were called Service Gateways.

---
##Service Broker API Overview
![inline](./assets/v2-broker-api.png)

---
##Service Plan
![inline](./assets/service-buy-plan.png)

---
##Service Broker Registration
- Make the service broker known to the Cloud Controller
```
cf create service-broker <broker name> <username> <password> <broker base URI>
```
Broker should ONLY allow access to those requestors it shared its credential with (Basic Auth)
See: [http://docs.gopivotal.com/pivotalcf/services/managing-service-brokers.html#register-broker](http://docs.gopivotal.com/pivotalcf/services/managing-service-brokers.html#register-broker)

---

<continued>
- Make ‘plans’ accessible to users in a specific org/space
Somewhat cumbersome: need to “hand parse” JSON to find service plan UUID as registered with the CC
See: [http://docs.gopivotal.com/pivotalcf/services/access-control.html#make-plans-public](http://docs.gopivotal.com/pivotalcf/services/access-control.html#make-plans-public)

__Need admin creds/role in order to introduce a service broker to the Cloud Controller!__

---
##Service Brokers
![inline](./assets/brokered-service-prov-bind.png)

---

##Service Broker Implementation Best Practice
Best Practice: Each binding is represented by its own credentials =>
- T=1: create service instance
  - __Neither App-1 or App-2 has access to the service instance__
- T=2: bind App-1 to service instance
  - __Only App-1 can access the service instance__
- T=3: bind App-2 to service instance
  - __Both App-1 and App-2 have access to the service instance__
- T=4 unbind App-1 from service instance
  - __Only App-2 can access the service instance__

---
##User Provided Service Instances
Typically legacy or __existing instances of a service__ (databases, queues, mail, etc) where applications connect to the __same instance__

__Credential passing__ when you need to inject the same credential set into an application

---
##User Provided Service Instances
![inline](./assets/user-provided-service-bind.png)

---
##Overview
![inline](./assets/services-env-overview.png)

---
##Summary
Deploying service instances and brokersPivotal CF only cares about the Service Broker URL.
- Several possibilities can be used for deploying services:  BOSH, vCAC, Puppet / Chef, etc  
- BOSH provides built-in resilience, VM monitoring and HA, multi-cloud support.
- For simpler cases, your own IaaS automation tool can be used.
- For quickly integration or when a Service Broker managing the service lifecycle is not desired, just use User-Provided Services.  (analogy to App Server Data Sources config)


---

---

PivotalCF Service Bindings
==========================

---

PivotalCF Service Bindings
==========================

Agenda
------

- What is a Service Binding in PivotalCF?
- `$VCAP_SERVICE` environment variable
- Connecting to services

---

What is a Service Binding in PivotalCF?
---------------------------------------

- Connection details & credentials
- Host & port
- Username & password
- Database name

---

Service Bindings
----------------

- Each application can be assigned multiple service bindings
- Must restart application
- Delivered to application via environment variable

```bash
$ cf bind-service my-app test-hdfs
$ cf restart my-app
```

---

`$VCAP_SERVICE` environment variable
------------------------------------

```json
{
  "hdfs": [
    {
      "name": "test-hdfs",
      "label": "hdfs",
      "tags": [

      ],
      "plan": "standard",
      "credentials": {
        "host": "att.hortonworks.com",
        "port": "50070"
      }
    }
  ]
}
```

---

No bound services
-----------------

- `$VCAP_SERVICES`

```json
{}
```

---

Connecting to services
----------------------

- Test for `$VCAP_SERVICE` from environment

```ruby
if ENV['VCAP_SERVICE']
```

- Parse JSON

```ruby
services = JSON.parse(ENV['VCAP_SERVICE'])
```

- Look the binding for the service you want

```ruby
hdfs = services.find { |name, s| s["label"] == "hdfs" }
```

- Construct connection

```ruby
if hdfs
  client = HDFSClient.new(hdfs["credentials"])
else
  raise "Please bind hdfs service"
end
```

---

Summary
-------

- Service bindings are not via config files
- Environment variables
- Application developers may have to do work

---

# PivotalCF Service Brokers

---

# **_Agenda_**

* What is a Service Broker?
* What does a Service Broker Provide?
* Service Broker API

---

# **_What is a Service Broker?_**

* A daemon process which exposes an HTTP API
* Implements the "Cloud Foundry Services API"
* "Cloud Foundry Services API" defines the contract between the Cloud Controller (CC) API and the Service Broker

---

# **_What is a Service Broker?_**

* May be implemented over HTTP or HTTPS
* May have a URI prefix
* May be load balanced
* May support Multiple Cloud Foundries

---

# **_What does a Service Broker Provide?_**

* A listing of Service Plans
* 'brokers' access to any number of Service Instances
* Creates Service Instances when a Service Plan is requested
* Returns credentials and dashboard URL (if one exists) for each service created

---

# **_Service Plan Catalog_**

* HTTP API endpoint:

GET /v2/catalog

* Requests the listing of known Service Plans provided by the Service Broker

---

# **_Provisioning_**

* HTTP API endpoint:

PUT /v2/service_instances/:id

* Creates a service instance for a Service Plan

---

# **_Deprovisioning_**

* HTTP API endpoint:

DELETE /v2/service_instances/:id

* Deprovisions (removes/deletes) a Service Instance

---

# **_Binding_**

* HTTP API endpoint:

PUT /v2/service_bindings/:id

* Binds an application to a Service Instance

---

# **_Unbinding_**

* HTTP API endpoint:

DELETE /v2/service_bindings/:id

* Unbinds an application from a Service Instance

---

![configuration](./assets/cf-service-brokers.png)

---

# **_Service Broker CLI_**

To obtain a list of available service brokers, with role Service Admin:

```bash
$ cf service-brokers
```

---

# **_Service Broker CLI_**

Service brokers can be created, updated, deleted, and renamed:

```bash
$ cf create-service-broker &lt;broker&gt; &lt;username&gt; &lt;password&gt; &lt;url&gt;

$ cf update-service-broker &lt;broker&gt; &lt;username&gt; &lt;password&gt; &lt;url&gt;

$ cf delete-service-broker &lt;broker&gt; &lt;username&gt; &lt;password&gt; &lt;url&gt;

$ cf rename-service-broker &lt;broker&gt; &lt;username&gt; &lt;password&gt; &lt;url&gt;
```

---

# **_Service Broker Lab_**

Lab "HashMap as a Service" Broker

[https://github.com/cf-platform-eng/cf-workshop-sb-module](https://github.com/cf-platform-eng/cf-workshop-sb-module)

Service wraps a minimal REST API around a Java HashMap implementation, and each service creation event results in a newly allocated Map.

Optional Lab

[https://github.com/pivotalservices/pse-training-cf-hw-service-broker](https://github.com/pivotalservices/pse-training-cf-hw-service-broker)

(Uses sample "Hello World" (Java Spring) service broker.)
