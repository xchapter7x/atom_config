0.1.1
=====

## Bug Fixes
- Fix preview link in README

0.1.0
=====

## New Features
- Support for all Base16 color schemes and styles.
- 'Smart' defaults: dark style for dark UI themes and light style for light UI themes.
- Preview of each selected theme on all open files while browsing through the list of available themes.
